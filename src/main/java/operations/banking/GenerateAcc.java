package operations.banking;

import java.util.Random;

public class GenerateAcc {
	 
	 public static String generateNumber(String prefix, int length){
	    Random rand = new Random();
	    StringBuilder sb = new StringBuilder();
	    sb.append(prefix);
	    for(int i = 0; i < length; i++) {
	    	sb.append(rand.nextInt(10));
	    }
	    
	    return sb.toString();
	  }
}
