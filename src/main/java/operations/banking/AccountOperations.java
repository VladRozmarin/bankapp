package operations.banking;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import accountMenu.BankAccount;
import databaseManagement.FileModifier;
import databaseManagement.FileReader;
import databaseManagement.LoginData;

public class AccountOperations {
	static Logger logger = LogManager.getLogger();
	private static String ACCOUNTS_FILE_NAME = "AccountList.txt";

	public void withdraw(double amount, double fee) {
		if (amount > 0) {
			amount = amount - fee;
		} else {
			logger.debug("You do not have sufficient funds.");
		}

	}

	public static void performAction(final String account, final String amount, final String currentUser, final String action) throws IllegalStateException{
		FileModifier.modifyAccountAmount(account, amount, currentUser, ACCOUNTS_FILE_NAME, action);

	}
	
	public static void transfer(final String accountTo, final String accountFrom, final String amount, final String currentUser,final String currencyFrom,final String currencyTo) throws ExceptionError{
		FileModifier.transfer(accountTo, accountFrom, Long.parseLong(amount), currentUser,ACCOUNTS_FILE_NAME);
	}
	
	public static void displayAccounts(LoginData userDetails) {
		logger.debug("Account list: ");
		List<BankAccount> accountList = FileReader.AccountListFromFile(ACCOUNTS_FILE_NAME);
		String owner = userDetails.getUserId();
		if(accountList.size()>0) {
		for (BankAccount account : accountList) 
			if(account.getOwner().equals(owner)) 
				logger.debug(account.toString());
	}else {
		logger.debug("no accounts to display");
	}
	}
}