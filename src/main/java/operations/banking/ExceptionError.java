package operations.banking;

public class ExceptionError extends Exception {

    public ExceptionError(String message) {
        super(message);
    }

    public ExceptionError(String message, Throwable throwable) {
        super(message, throwable);
    }


}