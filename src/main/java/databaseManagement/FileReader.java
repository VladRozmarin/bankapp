package databaseManagement;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import accountMenu.BankAccount;

import java.util.ArrayList;
import java.util.List;


public class FileReader {
	
	static Logger logger= LogManager.getLogger();
	
	public static List<LoginData> UserListFromFile(String fileName) {

			List<LoginData> userList = new ArrayList<LoginData>();
			// read from file from line to line
			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

				stream.forEach(l -> {
					String[] splitData = l.split(" ");
					userList.add(new LoginData(splitData[0],splitData[1]));

				});

			} catch (IOException e) {
				logger.debug("Error");
				// catch exception and print stack trace
			}
			return userList;
	}
	
	public static List<BankAccount> AccountListFromFile(String fileName) {
		List<BankAccount> accountList = new ArrayList<BankAccount>();
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(l -> {
				String[] splitData = l.split(" ");
				accountList.add(new BankAccount(splitData[0],splitData[1],splitData[2],Long.parseLong(splitData[3])));
			});

		} catch (IOException e) {
			logger.debug("Error");
			// catch exception and print stack trace
		}
		return accountList;
	}
}
