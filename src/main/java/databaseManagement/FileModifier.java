package databaseManagement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Raindrop
 *
 */
public class FileModifier {
	private static Logger logger = LogManager.getLogger();
	private static final String USER_DIR_PROPERTY_NAME = "user.dir";

	public static void writeToFile(String fileName, String content) {
		// get current project directory
		File file = new File(System.getProperty(USER_DIR_PROPERTY_NAME) + "/" + fileName);
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file, true));
			writer.append(content);
			writer.newLine();
			writer.close();
		} catch (Exception e) {
			logger.debug("error");
		}
	}
	
	public static void replaceInFile(String valueFrom, String valueTo, final String accountsFile) {
		File file = new File(System.getProperty(USER_DIR_PROPERTY_NAME) + "/" + accountsFile);
		Path path = file.toPath();
		Charset charset = StandardCharsets.UTF_8;

		String content;
		try {
			content = new String(Files.readAllBytes(path), charset);
			content = content.replaceAll(valueFrom, valueTo);
			Files.write(path, content.getBytes(charset));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void modifyAccountAmount(final String account, final String amount, final String currentUser, final String accountsFile, final String operation) throws IllegalStateException{
		File file = new File(System.getProperty(USER_DIR_PROPERTY_NAME) + "/" + accountsFile);
		logger.debug(file.toPath().toString());
		logger.debug(String.format("Params: account : %s, amount: %s, current user: %s, operation %s", account, amount, currentUser, operation));
		
		try (Stream<String> stream = Files.lines(file.toPath())) {
			stream.forEach(l -> {
				String[] splitData = l.split(" ");
				final String currentUserFromFile = splitData[1];
				if(currentUserFromFile.equals(currentUser)) {
					final String accountFromFile = splitData[0];
					if(accountFromFile.equals(account)) {
						final String amountFromFile = splitData[3];
						final Long amountFromFileDouble = Long.parseLong(amountFromFile);
						final Long amountToProcessDouble = Long.parseLong(amount);
						Long amountAfterOperation = null;
						if(operation.equals("D")) {
							amountAfterOperation = amountFromFileDouble + amountToProcessDouble;
						}else if (operation.equals("W")) {
							if(amountFromFileDouble<amountToProcessDouble) {
								throw new IllegalStateException("Amount to withdraw is larger than current balance.");
							}
							amountAfterOperation = amountFromFileDouble - amountToProcessDouble;
						}
						String lineToReplace = l.toString();
						int start = lineToReplace.lastIndexOf(amountFromFile);
						String lineToReplaceWith = lineToReplace.substring(0, start);
						lineToReplaceWith += amountAfterOperation;
						
						FileModifier.replaceInFile(lineToReplace, lineToReplaceWith, accountsFile);
						logger.debug(String.format("Replaced in file amount %s with %s", amountFromFile, amountAfterOperation));
					}
					
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void transfer(final String accountTo, final String accountFrom, final Long amount, final String currentUser, final String accountsFile){
		//widthdraw amount from accountFrom
		FileModifier.modifyAccountAmount(accountFrom, amount.toString(), currentUser, accountsFile, "W");
		//deposit amount to accountTo
		FileModifier.modifyAccountAmount(accountTo, amount.toString(), currentUser, accountsFile, "D");
	}

}
