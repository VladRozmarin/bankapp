package com.codebind;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.List;

public class FileReader {
	public List<LoginData> readFromFile(String fileName) {
		List<LoginData> loginData = new ArrayList<LoginData>();
		{
			// read from file from line to line
			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

				stream.forEach(l -> {
					String[] splitData = l.split(" ");
					loginData.add(new LoginData(splitData[0],splitData[1]));

				});

			} catch (IOException e) {
				e.printStackTrace();
				// catch exception and print stack trace
			}

			return loginData;
		}

	}

}
