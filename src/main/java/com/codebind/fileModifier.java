package com.codebind;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class FileModifier {
	public void writeToFile(String fileName) {
		// get current project directory
		File file = new File(System.getProperty("user.dir") + "/" + fileName);
		Logger logger = LogManager.getLogger();
		Scanner scan = new Scanner(System.in);
		String text = scan.nextLine();
		BufferedWriter writer = null;
		try {
			file.createNewFile();
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(text);
			writer.newLine();
			writer.close();
		} catch (Exception e) {
			logger.debug("error");
		}
	}

}
