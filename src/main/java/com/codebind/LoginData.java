package com.codebind;


public class LoginData {


	private String userId;
	private String pass;
	
	
	
	public LoginData(String userId, String pass) {
		this.userId = userId;
		this.pass = pass;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	
	@Override
	public boolean equals(Object o) {
	    if (o == this)
	        return true;
	    if (!(o instanceof LoginData))
	        return false;
	    LoginData other = (LoginData)o;
	    other.getInstance(userId);
	    other.getInstance(pass);
		return true;

	}
	private void getInstance(String userId) {
		// TODO Auto-generated method stub
		
	}
	

}
