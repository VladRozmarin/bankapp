package com.codebind;

import java.io.Writer;
import java.math.BigDecimal;
import java.util.Scanner;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

//class created for transfering amount of money to other accounts
public class Action{
	private static Logger logger = LogManager.getLogger();
	
	Scanner money= new Scanner(System.in);
	public static void transfer (Account from, Account to, double amount, double fee)
	{
	    from.withdraw(amount, fee);
	    to.deposit(amount);
	}
	
	public static void main(String[] args)
	{
	    // This creates two different accounts :)
	    BankAccount a = new BankAccount(userId);
	    BankAccount b = new BankAccount(userId);

	    // Tranfer
	    a.withdraw(amount);
	    logger.debug(a.getBalance());
	    b.deposit(amount);
	    logger.debug(b.getBalance());
	}

    }
 
