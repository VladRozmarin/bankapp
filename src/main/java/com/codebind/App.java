package com.codebind;

import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.Logger;

import accountMenu.BankAccount;
import databaseManagement.FileModifier;
import databaseManagement.FileReader;
import databaseManagement.LoginData;
import operations.banking.AccountOperations;
import operations.banking.ExceptionError;
import operations.banking.GenerateAcc;

import org.apache.logging.log4j.LogManager;

public class App {
	private static Logger logger = LogManager.getLogger();
	private static String PASSWORD_FILE_NAME = "UserPass.txt";

	public static void main(String[] args) {
		List<LoginData> loginEntry = FileReader.UserListFromFile(PASSWORD_FILE_NAME);
		String programStatus = "running";
		LoginData accountUser = new LoginData();
		Scanner input = new Scanner(System.in); // Create a Scanner object
		boolean appfunctionning = true;
		boolean decision = false;
		do {
			if (appfunctionning) {
				logger.debug("Welcome to your Bank App!");
				logger.debug("D: Login");
				logger.debug("E: Exit");

			} else if (decision) {
				logger.debug("A: Create Account");
				logger.debug("C: Display Accounts");
			} else {
				logger.debug("B: Account");
				logger.debug("F: Logout");
			}
			logger.debug("Please specify your option: ");
			String option = input.next();

			switch (option) {
			// create account option
			case "A":
				String iban = null;
				String accountType = null;
				do {
					logger.debug("Please specify your account type(EUR/RON): ");
					accountType = input.next();
					if (accountType.equals("EUR")) {
						iban = GenerateAcc.generateNumber("EU", 22);
					} else if (accountType.equals("RON")) {
						iban = GenerateAcc.generateNumber("RO", 22);
					}
					logger.debug(iban);
				} while (!(accountType.equals("EUR") || accountType.equals("RON")));
				logger.debug("Creating new account and writing it to file");
				BankAccount newAccount = new BankAccount(iban, accountUser.getUserId(), accountType, 0L);
				FileModifier.writeToFile("AccountList.txt", newAccount.toFileFormat());
				break;
			// option to deposit/withdraw money
			case "B":
				// get option from user input
				logger.debug("What action do you want to perform?");
				logger.debug("D: Deposit");
				logger.debug("W: Widthdraw");
				logger.debug("T: Transfer");
				String userAction = input.next();
				// get account from user input
				if (userAction.equals("D") || userAction.equals("W")) {
					logger.debug(String.format("Please input user account you want to %s %s",
							userAction.equals("D") ? "deposit" : "widthdraw", userAction.equals("D") ? "to" : "from"));
					AccountOperations.displayAccounts(accountUser);
					String userAccountInput = input.next();
					// get amount to perform action with
					logger.debug(String.format("Please input the amount you want to %s %s?",
							userAction.equals("D") ? "deposit" : "widthdraw", "asd"));
					String amountInput = input.next();
					try{
						AccountOperations.performAction(userAccountInput, amountInput, accountUser.getUserId(), userAction);	
					}catch (IllegalStateException e) {
						logger.debug(e.getMessage());
					}
				}
				// option to transfer money from one account to another
				else if (userAction.equals("T")) {
					String currencyFrom="RON";
					String currencyTo="RON";
					ExceptionError errorMessage= new ExceptionError("Make sure that your currency matches the target.");
					AccountOperations.displayAccounts(accountUser);
					logger.debug(String.format("Please enter the account you want to transfer from"));
					String accountFrom = input.next();
					logger.debug(String.format("Please enter the account you want to transfer to"));
					String accountTo = input.next();
					logger.debug(String.format("Please enter the amount you want to transfer"));
					String amountToTransfer = input.next();
					try{
						AccountOperations.transfer(accountTo, accountFrom, amountToTransfer, accountUser.getUserId(),currencyFrom,currencyTo);						
					}catch (ExceptionError MyError) {
						logger.debug(errorMessage.getMessage());
					}
				}
				break;
			case "C":
				// Display Accounts
				AccountOperations.displayAccounts(accountUser);
				break;
			case "D":
				// login

				logger.debug("Enter username: ");
				String userName = input.next(); // Read user input
				System.out.println("Enter password: ");
				String password = input.next();
				// validation user needed
				LoginData connectedUser = new LoginData(userName, password);
				if (loginEntry.contains(connectedUser)) {
					logger.debug("Welcome " + userName + "!");
					appfunctionning = false;
					accountUser = connectedUser;
				} else {
					logger.debug("Wrong username/password!");

				}
				break;
			case "E":
				programStatus = "exit";
				input.close();
				break;
			case "F":
				// logout
				appfunctionning = true;
				logger.debug("Succesfully logged out!");
				break;
			}

		} while (programStatus != "exit");
	}
}
