package accountMenu;

import java.util.Scanner;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class BankAccount {
	private static Logger logger = LogManager.getLogger();
	Scanner input = new Scanner(System.in);
	private String iban;
	private String owner;
	private String accountType;
	private Long balance;

	public BankAccount(String iban, String owner, String accountType, Long accountbalance) {
		this.iban = iban;
		this.owner = owner;
		this.accountType = accountType;
		this.balance = accountbalance;
	}

	@Override
	public String toString() {
		return "BankAccount [iban=" + iban + ", accountType=" + accountType + ", balance=" + balance + "]";
	}

	public String getOwner() {
		return owner;
	}

	void display() {
		logger.debug("UserName is: " + owner);
		logger.debug("Balance is: " + balance);

	}

	void dbalance() {
		logger.debug("Balance:" + balance);
	}

	public char[] getBalance() {
		return null;
	}

	public void withdraw(int withdrawnAmount) {
		balance = balance - withdrawnAmount;
	}

	public void deposit(int depositedAmount) {
		balance = balance + depositedAmount;
	}
	
	public String toFileFormat() {
		return iban + " " + owner + " " + accountType + " " + balance;
	}
}
